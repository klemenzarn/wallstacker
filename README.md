# WallStacker
simple wallstacker extension for freewall.js library (only when you have items in columns same width. columns can be different sizes)

example in GIF:

![alt tag](https://github.com/klemenzarn/WallStacker/blob/master/img/gif-demo.gif?raw=true)

live on page: https://kainoto.com

simple demo example: https://klemenzarn.github.io/WallStacker/

dependency:

-> jquery

-> freewall.js - https://github.com/kombai/freewall

usage:
```
//init freewall with auto height 
var freewall = new Freewall("#freewall");
freewall.reset({
        animate: false, //you can try it with 'true' but I recommend false
        gutterX: 1,
        gutterY: 1,
        width: 180,
        fixSize: 0,
        selector: '.freewallItem',
        onResize: function () {
            if (wallStacker != null)
                wallStacker.stop();
            freewall.fillHoles(); //optionally
            freewall.fitWidth();
        },
        onComplete: function () {
            //you can try to init wallstacker here too
        }
    });

//init wall stacker freewall extension
var wallStacker = new WallStacker(freewall);
wallStacker.start();
```

on window resize event:
```
var resizetimeout;
$(window).resize(function () {
    if (resizetimeout != undefined) clearTimeout(resizetimeout);

    resizetimeout = setTimeout(function () {
        if (wallStacker != null) {
            wallStacker.reset();
        }
    }, 500);
});
```
Live demo use default 180px width for each column. I will add examples with auto height wall items (pinterest-like layout: http://kombai.github.io/freewall/example/pinterest-layout.html) soon.
