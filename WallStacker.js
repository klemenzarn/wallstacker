/**
 * class WallStacker
 * @param {Object} wall
 */
function WallStacker(wall) {
    this.wallElementsArray = [];
    this.columnsData = [];
    this.navigationWall = wall;
    this.zIndex = 300;
    this.mostBottomPosition = 0;
    this.previousScroll = 0;
    this.stackBottomOffset = 50;
    this.previousMoveSize = 0;
    this.offsetArray = [];
}


// class logic
WallStacker.prototype = {
    constructor: WallStacker,

    /**
     * reset data for on resize event for example
     */
    reset: function() {
        this.wallElementsArray = [];
        this.columnsData = [];
        this.mostBottomPosition = 0;
        this.previousScroll = 0;
        this.previousMoveSize = 0;
        this.offsetArray = [];
        this.start();
    },

    /**
     * for unsetting custom atributes and css
     */
    stop: function() {
        var self = this;

        self.wallElementsArray.forEach(function(element, index) {
            var elementAttrs = element.attributes;

            var textElementAttr = element.textElement.attributes;
            var stackArray = self.columnsData[elementAttrs.offset.left];
            element.css({
                'position': 'absolute',
                'left': elementAttrs.left,
                'top': elementAttrs.top,
                'height': elementAttrs.height + 'px',
                'z-index': 'auto'
            });

            element.isStacked = false;
            element.stackingPosition = undefined;
            stackArray.pop(); //popamo zadnjo vrednost

            var currentStackSize = stackArray[stackArray.length - 1];
            self.mostBottomPosition = currentStackSize;
        });
    },


    /**
     * init function
     */
    start: function() {
        var self = this;

        var items = self.navigationWall.container.find('.contentListFrameItem');

        var allLeftOffsets = [];

        items.each(function(index) {
            var element = $(this);

            allLeftOffsets.push(element.offset().left);

            element.attributes = {
                width: element.width(),
                height: element.height(),
                top: element.css('top'),
                left: element.css('left'),
                offset: {
                    top: element.offset().top,
                    left: element.offset().left
                }
            };

            var textElement = element.find('.contentListFrameShortContent');

            textElement.attributes = {
                width: textElement.width(),
                height: textElement.outerHeight(),
                top: textElement.css('top'),
                left: textElement.css('left'),
                offset: {
                    top: textElement.offset().top,
                    left: textElement.offset().left
                }
            };

            element.textElement = textElement;
            element.isStacked = false; //default value is false

            self.wallElementsArray.push(element);
        });

        self.wallElementsArray.forEach(function(element) {
            //console.log(element.textElement.attributes.offset);
        });

        //filtering array of left offsets
        var uniqueLeftOffsets = [];
        $.each(allLeftOffsets, function(i, el) {
            if ($.inArray(el, uniqueLeftOffsets) === -1) uniqueLeftOffsets.push(el);
        });

        //stacking position data store
        for (var i = 0; i < uniqueLeftOffsets.length; i++) {
            self.columnsData[uniqueLeftOffsets[i]] = [0];
        }

        //simulating scroll, so no flickering
        this.simulateScrollCalculation(0, $(window).scrollTop());

        this.initScrollEvent();
    },

    getScrollDirection: function(previousScroll, currentScroll) {
        return previousScroll < currentScroll; // true -> down, false -> up
    },

    checkStackReposition: function(scrollPosition) {
        self = this;

        var windowHeight = $(window).height();
        var isScrollingDown = self.getScrollDirection(self.previousScroll, scrollPosition);
        if (isScrollingDown) {
            //logic for scrolling up when height of wall > height of window

            if (self.mostBottomPosition > windowHeight - self.stackBottomOffset) {
                var movingHeight = self.mostBottomPosition - windowHeight - self.previousMoveSize;
                self.previousMoveSize = movingHeight;
                console.log('ss', movingHeight);

                if (movingHeight > 0) {
                    self.offsetArray.push(movingHeight);
                    //console.log('starting moving stack up for', movingHeight);
                    self.moveStack(-movingHeight);
                    self.mostBottomPosition -= movingHeight;

                    for (var columnKey in self.columnsData) {
                        if (self.columnsData.hasOwnProperty(columnKey)) {
                            self.columnsData[columnKey].forEach(function(stackingPos, index) {
                                self.columnsData[columnKey][index] = stackingPos - movingHeight;
                            });
                        }
                    }

                }
            }
        } else {
            //logic for scrolling down when height of wall > height of window
            var isOriginalState = true;
            for (var columnKey in self.columnsData) {
                if (self.columnsData.hasOwnProperty(columnKey)) {
                    if (self.columnsData[columnKey][0] < 0) {
                        isOriginalState = false;
                        break;
                    }
                }
            }

            if (isOriginalState == false && self.mostBottomPosition < windowHeight - self.stackBottomOffset) {
                var movingHeight = self.offsetArray.pop();
                self.previousMoveSize = movingHeight;
                console.log('starting moving stack down for', movingHeight);
                self.moveStack(movingHeight);
                self.mostBottomPosition += movingHeight;
                for (var columnKey in self.columnsData) {
                    if (self.columnsData.hasOwnProperty(columnKey)) {
                        self.columnsData[columnKey].forEach(function(stackingPos, index) {
                            self.columnsData[columnKey][index] = stackingPos + movingHeight;
                        });
                    }
                }
            }

        }

    },

    //window on scroll event
    initScrollEvent: function() {
        var self = this;
        self.previousScroll = $(window).scrollTop()
        self.previousMoveSize = 0;

        $(window).scroll(function() {
            var scrollPosition = $(this).scrollTop();

            //for fast scroll we simulate it and prevent flickering for unstacking
            if (scrollPosition - self.previousScroll > 50) {
                self.simulateScrollCalculation(self.previousScroll, scrollPosition);
            } else {
                self.scrollCalculation(scrollPosition);
            }

            self.previousScroll = scrollPosition;
        });
    },

    //simulation of scrolling
    simulateScrollCalculation: function(start, end) {
        for (var i = start; i < end; i += 5) {
            this.scrollCalculation(i);
        }
    },

    //logic for stacking
    scrollCalculation: function(scrollPosition) {
        var self = this;

        self.checkStackReposition(scrollPosition);

        self.wallElementsArray.forEach(function(element, index) {
            var elementAttrs = element.attributes;

            var textElementAttr = element.textElement.attributes;
            var stackArray = self.columnsData[elementAttrs.offset.left];

            if (element.isStacked == false) {

                var stackSize = stackArray[stackArray.length - 1];

                if (scrollPosition + stackSize >= textElementAttr.offset.top) {

                    var elementStackHeight = textElementAttr.height;

                    element.css({
                        'position': 'fixed',
                        'left': elementAttrs.offset.left,
                        'top': stackSize,
                        'height': elementStackHeight + 'px',
                        'z-index': self.zIndex - index
                    })

                    var currentPositionOffset = elementStackHeight + stackSize + 1;
                    if (currentPositionOffset > self.mostBottomPosition) {
                        self.mostBottomPosition = currentPositionOffset;
                    }
                    self.columnsData[elementAttrs.offset.left].push(currentPositionOffset);

                    element.isStacked = true;
                    element.stackingPosition = scrollPosition;
                    element.attr('data-style', 'showFull');
                }
            } else {
                if (scrollPosition < element.stackingPosition) {
                    element.css({
                        'position': 'absolute',
                        'left': elementAttrs.left,
                        'top': elementAttrs.top,
                        'height': elementAttrs.height + 'px',
                        'z-index': 'auto'
                    });

                    element.isStacked = false;
                    element.stackingPosition = undefined;
                    stackArray.pop();

                    var currentStackSize = stackArray[stackArray.length - 1];
                    self.mostBottomPosition = currentStackSize;
                }

            }
        });


    },

    /**
     * moving stack up and down
     */
    moveStack: function(offset) {
        console.log('moving for', offset);
        this.wallElementsArray.forEach(function(element, index) {
            //element.css({
            //    'transform': 'translateY(' + offset + '%)'
            //});
            var currentTopPosition = parseInt(element.css('top').replace('px', ''));
            //var currentTopPosition = parseInt(element.attributes.top.replace('px', ''));
            //console.log(currentTopPosition);
            element.css({
                'top': (offset + currentTopPosition) + 'px'
            })
            element.stackingPosition += offset;
        });
    }
};